#
# Cookbook:: scm
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.

template '/etc/motd' do 
 source "motd.erb"
 variables({
   :persona =>node['scm']['nombre']})
end

template '/etc/miaplicacion.conf' do
  source "miaplicacion.conf.erb"
  variables({
  :IP => node['scm']['IP'],
  :USER => node['scm']['USER'],
  :PORT => node['scm']['PORT']
  })
end
